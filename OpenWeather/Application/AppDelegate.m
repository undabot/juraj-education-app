#import "AppDelegate.h"
#import "ApplicationViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    ApplicationViewController *applicationViewController = [ApplicationViewController new];
    self.window.rootViewController = applicationViewController;    
    return YES;
}


@end
